//
//  main.m
//  Home Shine
//
//  Created by Purplechai Mac One on 5/31/16.
//  Copyright © 2016 PurpleChai Mac One. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
