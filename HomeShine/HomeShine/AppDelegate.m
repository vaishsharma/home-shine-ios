//
//  AppDelegate.m
//  Home Shine
//
//  Created by Purplechai Mac One on 5/31/16.
//  Copyright © 2016 PurpleChai Mac One. All rights reserved.
//

#import "AppDelegate.h"
#import "SelectLocationViewController.h"
#import "ViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"bg_640x1136.jpg"] forBarMetrics:UIBarMetricsDefault];
    
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}]; 
    
    
    NSString * plistSourcePath = [[NSBundle mainBundle] pathForResource:@"PropertyList" ofType:@"plist"];
    
    NSString * plistDestinationPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/PropertyList.plist"];
    
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistDestinationPath])
    {
        [[NSFileManager defaultManager] copyItemAtPath:plistSourcePath toPath:plistDestinationPath error:nil];
    }
    
    NSDictionary * savedDetails = [NSDictionary dictionaryWithContentsOfFile:plistDestinationPath];

    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];      //initialising the view with the main screen frame
    
    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController * vc;
    
    if ([[savedDetails valueForKey:@"userLocation"] isEqualToString:@""])
    {
        vc = [storyBoard instantiateViewControllerWithIdentifier:@"LocationViewController"];
//        vc.navigationController.navigationBarHidden = YES;
    }
    
    else
    {
        vc = [storyBoard instantiateViewControllerWithIdentifier:@"homeScreen"];
//        vc.navigationController.navigationBarHidden = YES;
    }
    
    [self.window setRootViewController:vc];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
