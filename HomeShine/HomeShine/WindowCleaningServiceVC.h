//
//  WindowCleaningServiceVC.h
//  Home Shine
//
//  Created by Purplechai Mac One on 6/7/16.
//  Copyright © 2016 PurpleChai Mac One. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WindowCleaningServiceVC : UIViewController <UITableViewDataSource, UITableViewDelegate>


@property (strong, nonatomic) IBOutlet UIButton *shoppingCart;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (strong, nonatomic) IBOutlet UIImageView *servicesIcon;
@property (weak, nonatomic) NSNumber *index;
@property (strong, nonatomic) IBOutlet UITextField *quantityTF;
@property (strong, nonatomic) IBOutlet UITableView *dropDownTable;

- (IBAction)dropDownAction:(UIButton *)sender;
- (IBAction)backAction:(UIBarButtonItem *)sender;
- (IBAction)addToCartAction:(UIButton *)sender;
- (IBAction)showItemsInCart:(UIButton *)sender;
- (IBAction)changeService:(UIButton *)sender;
- (IBAction) addItem:(id)sender;
- (IBAction) deleteItem:(id)sender;


@end
