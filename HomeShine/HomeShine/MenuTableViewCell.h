//
//  MenuTableViewCell.h
//  Home Shine
//
//  Created by Purplechai Mac One on 6/3/16.
//  Copyright © 2016 PurpleChai Mac One. All rights reserved.
//

//Custom cell to show User image & name in Menu-slide bar


#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIImageView *profileImage;

@property (strong, nonatomic) IBOutlet UILabel *userName;


@end
