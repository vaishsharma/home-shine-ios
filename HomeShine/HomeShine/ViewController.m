//
//  ViewController.m
//  Home Shine
//
//  Created by Purplechai Mac One on 5/31/16.
//  Copyright © 2016 PurpleChai Mac One. All rights reserved.
//

#import "ViewController.h"
#import "ServicesViewController.h"
#import "ChairCleaningServiceVC.h"
#import "FridgeCleaningServiceVC.h"
#import "MattressCleaningServiceVC.h"
#import "SofaCleaningServiceVC.h"
#import "WashroomCleaningServiceVC.h"
#import "WindowCleaningServiceVC.h"
#import "SWRevealViewController.h"

@interface ViewController ()
{
    NSMutableDictionary * savedDetails;
    NSUserDefaults *userData;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    userData = [NSUserDefaults standardUserDefaults];
    _currentLocation.text = self.thisLocation;
        
    SWRevealViewController * revealViewController = self.revealViewController;
    
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    NSString * plistDestinationPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/PropertyList.plist"];
    
    savedDetails = [NSMutableDictionary dictionaryWithContentsOfFile:plistDestinationPath];
    
    [savedDetails setValue:[NSNumber numberWithBool:NO] forKey:@"hasUserLoggedIn"];
    
    _currentLocation.text = [savedDetails valueForKey:@"userLocation"];
    
    NSLog(@"%@", savedDetails);
    
    [savedDetails writeToFile:plistDestinationPath atomically:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)changeLocation:(UIButton *)sender
{
    SelectLocationViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"LocationViewController"];
    [self.navigationController pushViewController:vc animated:TRUE];
}

- (IBAction)serviceSelected:(UIButton *)sender
{
  
    NSString *currentService = @"";
    id vc;
    switch (sender.tag)
    {
        case 100:
        {
            currentService = @"Carpet";
            vc = (ServicesViewController *) [self.storyboard instantiateViewControllerWithIdentifier:[currentService lowercaseString]];
//            [self.navigationController pushViewController:vc animated:YES];

            break;
        }
        
        case 101:
        {
            currentService = @"Chair";
            vc = (ChairCleaningServiceVC* )[self.storyboard instantiateViewControllerWithIdentifier:[currentService lowercaseString]];
//            [self.navigationController pushViewController:vc animated:YES];
            break;
            
        }
            
        case 102:
        {
            currentService = @"Fridge";
            vc = (FridgeCleaningServiceVC *)[self.storyboard instantiateViewControllerWithIdentifier:[currentService lowercaseString]];
//            FridgeCleaningServiceVC * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FridgeServicesVC"];
//            [self.navigationController pushViewController:vc animated:YES];
            break;
            
        }
        
        case 103:
        {
            currentService = @"Matress";
            vc = (MattressCleaningServiceVC *)[self.storyboard instantiateViewControllerWithIdentifier:[currentService lowercaseString]];
//            MattressCleaningServiceVC * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MattressVC"];
//            [self.navigationController pushViewController:vc animated:YES];
            break;
            
        }
            
        case 104:
        {
            currentService = @"Sofa";
            vc = (SofaCleaningServiceVC *)[self.storyboard instantiateViewControllerWithIdentifier:[currentService lowercaseString]];
//            SofaCleaningServiceVC * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SofaVC"];
//            [self.navigationController pushViewController:vc animated:YES];
            break;
            
        }
            
        case 105:
        {
            currentService = @"Washroom";
            vc = (WashroomCleaningServiceVC *)[self.storyboard instantiateViewControllerWithIdentifier:[currentService lowercaseString]];
//            WashroomCleaningServiceVC * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"WashroomServicesVC"];
//            [self.navigationController pushViewController:vc animated:YES];
            break;
            
        }
        
        case 106:
        {
            currentService = @"Window";
            vc = (WindowCleaningServiceVC *)[self.storyboard instantiateViewControllerWithIdentifier:[currentService lowercaseString]];
            break;
            
        }
            
        default:
            break;
    }
    
    [userData setObject:currentService forKey:@"currentService"];
    [userData synchronize];
    
    [self.navigationController pushViewController:vc animated:YES];
//    [self.navigationController pushViewController:vc animated:YES];
}
@end
