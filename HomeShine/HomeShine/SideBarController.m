//
//  SideBarController.m
//  Home Shine
//
//  Created by Purplechai Mac One on 6/2/16.
//  Copyright © 2016 PurpleChai Mac One. All rights reserved.
//

#import "SideBarController.h"
#import "MenuTableViewCell.h"
#import "SWRevealViewController.h"

@interface SideBarController ()
{
    NSArray * guestOptions, * loggedUserOptions, *otherOptions;
    NSDictionary * savedDetails;
}

@end

@implementation SideBarController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString * plistDestinationPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/PropertyList.plist"];
    
    savedDetails = [NSDictionary dictionaryWithContentsOfFile:plistDestinationPath];
    
    guestOptions = @[@"Login"];
    
    otherOptions = @[@"Privacy", @"Home Shine v1.0.0"];
    
    loggedUserOptions = @[@"Change Password", @"Items in Cart", @"Order History", @"LogOut"];
    
    NSLog(@"%@", savedDetails);

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//    if ([[savedDetails valueForKey:@"hasUserLoggedIn"] boolValue] == NO)
//    {
//        return 2;
//    }
//    
//    else
//    return 3;
    
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 1;
    }
    else if((section == 1) && ([[savedDetails valueForKey:@"hasUserLoggedIn"] boolValue] == NO))
        return guestOptions.count;
    
    else if((section == 1) && ([[savedDetails valueForKey:@"hasUserLoggedIn"] boolValue] == YES))
        return loggedUserOptions.count;
    
    else
        return otherOptions.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath section] == 0)
    {
        MenuTableViewCell * dpCell = [tableView dequeueReusableCellWithIdentifier:@"dpCell" forIndexPath:indexPath];
        
        dpCell.userName.text = @"GUEST USER";
        dpCell.profileImage.image = [UIImage imageNamed:@"logo_iside.png"];
        dpCell.profileImage.layer.cornerRadius = 50;
        dpCell.profileImage.clipsToBounds = YES;
        
        return dpCell;
    }
    
    else if((indexPath.section == 1) && ([[savedDetails valueForKey:@"hasUserLoggedIn"] boolValue] == NO))
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        
        cell.textLabel.text = [guestOptions objectAtIndex:indexPath.row];
        
//        if ([[guestOptions objectAtIndex:indexPath.row] isEqualToString:[guestOptions lastObject]])
//        {
//           // cell.textLabel.textAlignment = NSTextAlignmentCenter;
//            cell.textLabel.textColor = [UIColor grayColor];
//            return cell;
//        }
        
        return cell;
    }
    
    else
    {
        if (indexPath.section == 1)
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            
            cell.textLabel.text = [loggedUserOptions objectAtIndex:indexPath.row];
            
            return cell;
        }
        
        else
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            
            cell.textLabel.text = [otherOptions objectAtIndex:indexPath.row];
            
            [cell.textLabel setFont:[UIFont systemFontOfSize:16]];
            
                    if ([[otherOptions objectAtIndex:indexPath.row] isEqualToString:[otherOptions lastObject]])
                    {
                       // cell.textLabel.textAlignment = NSTextAlignmentCenter;
                        cell.textLabel.textColor = [UIColor grayColor];
                        return cell;
                    }

            return  cell;
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        return 150;
    }
    
    else
        return 44;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
