//
//  SelectLocationViewController.h
//  Home Shine
//
//  Created by Purplechai Mac One on 6/2/16.
//  Copyright © 2016 PurpleChai Mac One. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface SelectLocationViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>


@property (strong, nonatomic) IBOutlet UITableView *locationsTableView;
- (IBAction)cancelAction:(id)sender;

@end
