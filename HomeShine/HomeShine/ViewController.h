//
//  ViewController.h
//  Home Shine
//
//  Created by Purplechai Mac One on 5/31/16.
//  Copyright © 2016 PurpleChai Mac One. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectLocationViewController.h"

@interface ViewController : UIViewController

@property (strong, nonatomic) NSString *thisLocation;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (strong, nonatomic) IBOutlet UILabel *currentLocation;

- (IBAction)changeLocation:(UIButton *)sender;
- (IBAction)serviceSelected:(UIButton *)sender;

@end

