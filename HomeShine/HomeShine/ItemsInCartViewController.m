//
//  ItemsInCartViewController.m
//  Home Shine
//
//  Created by Purplechai Mac One on 6/6/16.
//  Copyright © 2016 PurpleChai Mac One. All rights reserved.
//

#import "ItemsInCartViewController.h"

@interface ItemsInCartViewController ()

@end

@implementation ItemsInCartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backAction:(UIBarButtonItem *)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)popView:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];

}
@end
