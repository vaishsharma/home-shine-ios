//
//  RegisterViewController.m
//  Home Shine
//
//  Created by Purplechai Mac One on 6/3/16.
//  Copyright © 2016 PurpleChai Mac One. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()
{
    int flag;
    UITextField * currentTF;
}

@end

@implementation RegisterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(animateViewUpward:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(animateViewDownwards:) name:UIKeyboardWillHideNotification object:nil];
    
    flag = 0;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    currentTF = textField;
   // [self animateViewUpward:UIKeyboardWillShowNotification];
}

-(void)animateViewDownwards:(NSNotification *)notification
{
    [UIView transitionWithView:self.view duration:0.3 options:UIViewAnimationOptionTransitionNone animations:^
     {
         [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
     } completion:nil];
}

-(void)animateViewUpward:(NSNotification*)notification
{
    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
//    if ((currentTF.tag == 103) || (currentTF.tag == 104) || (currentTF.tag == 105) || (currentTF.tag == 106) || (currentTF.tag == 107))
//    {
//        [UIView transitionWithView:self.view duration:0.3 options:UIViewAnimationOptionTransitionNone animations:^
//         {
//             [self.view setFrame:CGRectMake(0, self.view.frame.origin.y-50, self.view.frame.size.width, self.view.frame.size.height)];
//         } completion:nil];
//    }
    
    if((currentTF.frame.origin.y+currentTF.frame.size.height) > (self.view.frame.size.height - keyboardRect.size.height))
    {
        [UIView transitionWithView:self.view duration:0.3 options:UIViewAnimationOptionTransitionNone animations:^
         {
             [self.view setFrame:CGRectMake(0, self.view.frame.origin.y-50, self.view.frame.size.width, self.view.frame.size.height)];
         } completion:nil];

    }
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    switch (textField.tag)
    {
        case 101:
            [_lastNameTF becomeFirstResponder];
            break;
            
        case 102:
            [_emailTF becomeFirstResponder];
            break;
            
        case 103:
            [_passwordTF becomeFirstResponder];
            break;
            
        case 104:
            [_mobileTF becomeFirstResponder];
            break;
        case 105:
            [_addressTF becomeFirstResponder];
            break;
        case 106:
            [self registerUser:self.registerButton];
            break;

        default:
            break;
    }
    
    
    return YES;
}


-(BOOL) isValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = TRUE;
    NSString *stricterFilterString = @"[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(BOOL) isValidMobile:(NSString *)checkString {
    NSString *stricterFilterString = @"(\\+|\\d)[0-9]{7,16}";
    NSString *mobileRegex = stricterFilterString;
    NSPredicate *mobileTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileRegex];
    return [mobileTest evaluateWithObject:checkString];
}


#pragma mark - Registration

- (IBAction)registerUser:(UIButton *)sender
{
    int errorCode;
    
    
    NSString * error = nil;
    UIAlertController *alertController;
    UIAlertAction *action;
    
    if (flag == 0)
    {
        errorCode =0;
        error = @"Please fill up all the fields marked as *";
        
        
        alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:error preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction * action1 = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [_firstNameTF becomeFirstResponder];
            
        }];
        
        [alertController addAction:action1];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    
    if (error == nil)
    {
        if ([[_firstNameTF.text stringByReplacingOccurrencesOfString:@" " withString:@""] length] == 0)
        {
            error = @"Name cannot be left blank!";
            errorCode =1;
            action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                [_firstNameTF becomeFirstResponder];
            }];
            alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:error preferredStyle:UIAlertControllerStyleAlert];
            
            [alertController addAction:action];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
        else
        {
            NSCharacterSet * alphabetSet=[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIKLMNOPQRSTUVWXYZ"];
            
            NSCharacterSet *all = [NSCharacterSet alloc];
            all = [alphabetSet invertedSet];
            
            NSRange range = [_firstNameTF.text rangeOfCharacterFromSet:all];
            
            if (range.location != NSNotFound)
            {
                error = @"Name should not contain numbers or special characters!";
                errorCode =2;
                action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    [_firstNameTF becomeFirstResponder];
                }];
                alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:error preferredStyle:UIAlertControllerStyleAlert];
                
                
                [alertController addAction:action];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
    }
    
    if (error == nil)
    {
        if ([[_passwordTF.text stringByReplacingOccurrencesOfString:@" " withString:@""] length] == 0)
        {
            error = @"Password field cannot be left empty";
            errorCode = 3;
            action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                [_passwordTF becomeFirstResponder];
            }];
            alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:error preferredStyle:UIAlertControllerStyleAlert];
            
            
            [alertController addAction:action];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
        else
        {
            if (_passwordTF.text.length < 8)
            {
                error = @"Please enter a Password with atleast 8 characters";
                errorCode = 4;
                action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    [_passwordTF becomeFirstResponder];
                }];
                alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:error preferredStyle:UIAlertControllerStyleAlert];
                
                
                [alertController addAction:action];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
    }
    
//    if (error == nil)
//    {
//        if (_confirmPasswordField.text.length == 0)
//        {
//            error = @"Please confirm your Password";
//            errorCode = 5;
//            action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//                [_confirmPasswordField becomeFirstResponder];
//            }];
//            alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:error preferredStyle:UIAlertControllerStyleAlert];
//            
//            
//            [alertController addAction:action];
//            [self presentViewController:alertController animated:YES completion:nil];
//            
//        }
//        
//        else
//        {
//            if (!([_passwordField.text isEqualToString:_confirmPasswordField.text]))
//            {
//                error = @"Password do not match";
//                errorCode = 6;
//                action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
//                    [_confirmPasswordField becomeFirstResponder];
//                }];
//                alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:error preferredStyle:UIAlertControllerStyleAlert];
//                
//                
//                [alertController addAction:action];
//                [self presentViewController:alertController animated:YES completion:nil];
//            }
//        }
//    }
    
    if (error == nil)
    {
        if ([[_emailTF.text stringByReplacingOccurrencesOfString:@" " withString:@""] length] == 0)
        {
            error = @"Email field cannot be left empty!";
            errorCode = 5;
            action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                [_emailTF becomeFirstResponder];
            }];
            alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:error preferredStyle:UIAlertControllerStyleAlert];
            
            
            [alertController addAction:action];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
        else
        {
            if (![self isValidEmail:_emailTF.text])
            {
                error = @"Enter valid email address";
                errorCode = 6;
                action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    [_emailTF becomeFirstResponder];
                }];
                alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:error preferredStyle:UIAlertControllerStyleAlert];
                
                
                [alertController addAction:action];
                [self presentViewController:alertController animated:YES completion:nil];
            }
        }
    }
    
    if (error == nil)
    {
        
        if ((_mobileTF.text.length ==0) || (_mobileTF.text.length > 10) || (![self isValidMobile:_mobileTF.text]))
        {
            error = @"Please Enter a valid mobile No.";
            errorCode = 7;
            action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                [_mobileTF becomeFirstResponder];
            }];
            alertController = [UIAlertController alertControllerWithTitle:@"Alert" message:error preferredStyle:UIAlertControllerStyleAlert];
            
            
            [alertController addAction:action];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
        
    }
    
    
    
    if (error == nil)
    {
        
        NSLog(@"\n\t Login Success!!");
        
        alertController = [UIAlertController alertControllerWithTitle:@"Success" message:@"Login Successful!" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction * action = [UIAlertAction actionWithTitle:@"Proceed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSLog(@"Logged in; Viewcontroller shud be loaded here");
        }];
        
        [alertController addAction:action];
        [self presentViewController:alertController animated:YES completion:nil];
    }

}
@end
