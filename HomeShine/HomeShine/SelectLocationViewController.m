//
//  SelectLocationViewController.m
//  Home Shine
//
//  Created by Purplechai Mac One on 6/2/16.
//  Copyright © 2016 PurpleChai Mac One. All rights reserved.
//

#import "SelectLocationViewController.h"
#import "SWRevealViewController.h"
#import "AFNetworking.h"


@interface SelectLocationViewController ()
{
    NSArray *locationList;
    NSMutableDictionary *savedDetails;
}
@end

@implementation SelectLocationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //[self.navigationController.navigationBar setHidden:YES];
//    [[UINavigationBar appearance] setHidden:YES];
//    [self.navigationController setNavigationBarHidden:YES];
    self.navigationItem.title = @"Select Location";
    
    NSString * url = [NSString stringWithFormat:@"http://www.homeshineindia.com/adminpanel/api/functions.php?action=allData"];
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
    {
        NSLog(@"%@", responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error = %@",error.localizedDescription);

    }];
    

    
    locationList = @[@"Pune", @"Thane", @"Satara", @"Nashik"];
    
    self.locationsTableView.dataSource =self;
    self.locationsTableView.delegate = self;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return locationList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.textLabel.text = [locationList objectAtIndex:indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * plistDestinationPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/PropertyList.plist"];
    
    savedDetails = [NSMutableDictionary dictionaryWithContentsOfFile:plistDestinationPath];
    [savedDetails setValue:[locationList objectAtIndex:indexPath.row] forKey:@"userLocation"];
    
  //  NSLog(@"%@", savedDetails);
    
    [savedDetails writeToFile:plistDestinationPath atomically:YES];

    SWRevealViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"homeScreen"];
    
   // vc.thisLocation = [locationList objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
//    [self presentViewController:vc animated:YES completion:nil];
}
- (IBAction)cancelAction:(id)sender {
//    UIViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"homeScreen"];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
