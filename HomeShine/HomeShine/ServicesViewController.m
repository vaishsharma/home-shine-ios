//
//  ServicesViewController.m
//  Home Shine
//
//  Created by Purplechai Mac One on 6/2/16.
//  Copyright © 2016 PurpleChai Mac One. All rights reserved.
//

#import "ServicesViewController.h"
#import "SWRevealViewController.h"
#import "ItemsInCartViewController.h"
#import "ChairCleaningServiceVC.h"
#import "FridgeCleaningServiceVC.h"
#import "MattressCleaningServiceVC.h"
#import "SofaCleaningServiceVC.h"
#import "WashroomCleaningServiceVC.h"
#import "WindowCleaningServiceVC.h"

@interface ServicesViewController ()
{
    NSArray * storyboardIDs;
    NSMutableArray *itemList;
    BOOL flag;
    NSDictionary * savedDetails;
    UILabel *badgeValue;
    NSString * plistPath;
    NSUserDefaults *userData;
}

@end

@implementation ServicesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    userData = [NSUserDefaults standardUserDefaults];
    self.navigationItem.title = @"Carpet Cleaning";
    [self.dropDownTable setHidden:YES];
    _dropDownTable.delegate = self;
    _dropDownTable.dataSource = self;
    
    flag = false;
    
    SWRevealViewController * revealViewController = self.revealViewController;
    
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }

    plistPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/PropertyList.plist"];
    
    savedDetails = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    
    itemList = [[NSMutableArray alloc]initWithArray:[savedDetails valueForKey:@"itemsInCart"]];

    
    if ([itemList count] != 0)
    {
        badgeValue = [[UILabel alloc]initWithFrame:CGRectMake(_shoppingCart.frame.origin.x+_shoppingCart.frame.size.width,_shoppingCart.frame.origin.y, 20, 20)];
        badgeValue.textColor = [UIColor whiteColor];
        badgeValue.textAlignment = NSTextAlignmentCenter;
        badgeValue.text = [NSString stringWithFormat:@"%lu",(unsigned long)itemList.count];
        badgeValue.layer.borderWidth = 1;
        badgeValue.layer.cornerRadius = 10;
        badgeValue.layer.masksToBounds = YES;
        badgeValue.layer.borderColor =[[UIColor whiteColor] CGColor];
        badgeValue.layer.shadowColor = [[UIColor clearColor] CGColor];
        badgeValue.layer.shadowOffset = CGSizeMake(0.0, 0.0);
        badgeValue.layer.shadowOpacity = 0.0;
        badgeValue.backgroundColor = [UIColor redColor];
        badgeValue.font = [UIFont fontWithName:@"ArialMT" size:12];
        
        [self.view addSubview:badgeValue];

    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (IBAction)dropDownAction:(UIButton *)sender
{
    
    if (flag == false)
    {
        flag = true;
        _dropDownTable.hidden = NO;
    }
    
    else
    {
        flag = false;
        _dropDownTable.hidden = YES;
    }
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2; //[titleList count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   // static NSString *CellIdentifier = @"cell";
    
   UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    // Set up the cell...
   // cell.textLabel.text = [titleList objectAtIndex:indexPath.row];
    [cell.textLabel setFont:[UIFont systemFontOfSize:14]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}


-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 20;
}



- (IBAction)showItemsInCart:(UIButton *)sender
{
    if ([[savedDetails valueForKey:@"hasUserLoggedIn"] boolValue] == YES)
    {
        ItemsInCartViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"cartVC"];
        
        [self.navigationController pushViewController:vc animated:YES];
        
        NSLog(@"User has logged In!!");
        
    }
    
    NSLog(@"Guest User!!");
}

- (IBAction)changeService:(UIButton *)sender {
    long int i = sender.tag + 100;
    
    NSString *currentService = @"";
    id vc;
    switch (i)
    {
        case 100:
        {
            currentService = @"Carpet";
            vc = (ServicesViewController *) [self.storyboard instantiateViewControllerWithIdentifier:[currentService lowercaseString]];
            break;
        }
            
        case 101:
        {
            currentService = @"Chair";
            vc = (ChairCleaningServiceVC* )[self.storyboard instantiateViewControllerWithIdentifier:[currentService lowercaseString]];
            break;
        }
            
        case 102:
        {
            currentService = @"Fridge";
            vc = (FridgeCleaningServiceVC *)[self.storyboard instantiateViewControllerWithIdentifier:[currentService lowercaseString]];
            break;
        }
            
        case 103:
        {
            currentService = @"Matress";
            vc = (MattressCleaningServiceVC *)[self.storyboard instantiateViewControllerWithIdentifier:[currentService lowercaseString]];
            break;
        }
            
        case 104:
        {
            currentService = @"Sofa";
            vc = (SofaCleaningServiceVC *)[self.storyboard instantiateViewControllerWithIdentifier:[currentService lowercaseString]];
            break;
        }
            
        case 105:
        {
            currentService = @"Washroom";
            vc = (WashroomCleaningServiceVC *)[self.storyboard instantiateViewControllerWithIdentifier:[currentService lowercaseString]];
            break;
        }
            
        case 106:
        {
            currentService = @"Window";
            vc = (WindowCleaningServiceVC *)[self.storyboard instantiateViewControllerWithIdentifier:[currentService lowercaseString]];
            break;
        }
            
        default:
            break;
    }
    
    [userData setObject:currentService forKey:@"currentService"];
    [userData synchronize];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)addToCartAction:(UIButton *)sender {
    if (_quantityTF.text.length != 0)
    {
        //add qauntity to cart
        
        [itemList addObject:_quantityTF.text];
        
        badgeValue.text = [NSString stringWithFormat:@"%lu",(unsigned long)itemList.count];

        if (itemList.count != 0)
        {
            [self.view addSubview:badgeValue];
        }
        
        [savedDetails setValue:itemList forKey:@"itemsInCart"];
        [savedDetails writeToFile:plistPath atomically:YES];
        
    }
}
- (IBAction)backAction:(UIBarButtonItem *)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
