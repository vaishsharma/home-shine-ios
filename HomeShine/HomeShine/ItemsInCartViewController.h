//
//  ItemsInCartViewController.h
//  Home Shine
//
//  Created by Purplechai Mac One on 6/6/16.
//  Copyright © 2016 PurpleChai Mac One. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface ItemsInCartViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

- (IBAction)backAction:(UIBarButtonItem *)sender;

- (IBAction)popView:(id)sender;

@end
